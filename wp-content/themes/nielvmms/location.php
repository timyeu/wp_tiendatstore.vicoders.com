<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

/*
Template Name: Blog
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $post;

get_header(); ?>

    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d50126.14932216474!2d105.82319751355885!3d20.966632842970977!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac044f17e86f%3A0xd0bd43581994298b!2zMjk2IE1pbmggS2hhaSwgSGFpIELDoCBUcsawbmcsIEjDoCBO4buZaSAxMDAwMCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1471318910669" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

<?php get_footer(); ?>